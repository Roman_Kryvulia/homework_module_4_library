USE [R_Kryvulia_Library]
GO
DROP TABLE IF EXISTS [author] 
GO
DROP TABLE IF EXISTS [book] 
GO
DROP TABLE IF EXISTS [book_author] 
GO
DROP TABLE IF EXISTS [publisher] 
GO
DROP TABLE IF EXISTS [author_log] 
GO


CREATE SEQUENCE  gen_number 
    as INT
	START WITH 10  
    INCREMENT BY 2 ;  
GO 
--next value for gen_number

CREATE TABLE [author] (
author_id INT PRIMARY KEY,
name NVARCHAR(50) NOT NULL,
URL VARCHAR(50) NOT NULL DEFAULT 'www.author_name.com',
insterted DATE NOT NULL DEFAULT GETDATE(),
insterted_by NVARCHAR(50) NOT NULL DEFAULT SYSTEM_USER,
updated DATE,
updated_by NVARCHAR(50),
CONSTRAINT UQ_author__name UNIQUE (name)
)
GO

CREATE TABLE [publisher] (
publisher_id INT PRIMARY KEY,
name NVARCHAR(50) NOT NULL,
URL VARCHAR(50) NOT NULL DEFAULT 'www.publisher_name.com',
insterted DATE NOT NULL DEFAULT GETDATE(),
insterted_by NVARCHAR(50) NOT NULL DEFAULT SYSTEM_USER,
updated DATE,
updated_by NVARCHAR(50),
CONSTRAINT UQ_publisher__name UNIQUE (name)
)
GO



CREATE TABLE [book] (
ISBN NVARCHAR(50) NOT NULL PRIMARY KEY,
publisher_id INT NOT NULL,
URL VARCHAR(50) NOT NULL,
price FLOAT NOT NULL DEFAULT (0),
insterted DATE NOT NULL DEFAULT GETDATE(),
insterted_by NVARCHAR(50) NOT NULL DEFAULT SYSTEM_USER,
updated DATE,
updated_by NVARCHAR(50),
CONSTRAINT UQ_book__URL UNIQUE (URL)
)
GO
ALTER TABLE [book]
 ADD CONSTRAINT CK_book_price CHECK (price>=0)
GO
ALTER TABLE [book]
WITH CHECK ADD  CONSTRAINT FK_book_publisher FOREIGN KEY(publisher_id)
REFERENCES [publisher] (publisher_id)
GO



CREATE TABLE [book_author] (
book_author_id INT NOT NULL PRIMARY KEY DEFAULT (1),
ISBN NVARCHAR(50) NOT NULL,
author_id INT NOT NULL,
seq_no INT NOT NULL DEFAULT (1),
insterted DATE NOT NULL DEFAULT GETDATE(),
insterted_by NVARCHAR(50) NOT NULL DEFAULT SYSTEM_USER,
updated DATE,
updated_by NVARCHAR(50),
CONSTRAINT UQ_book_author__ISBN UNIQUE (ISBN),
CONSTRAINT UQ_book_author__author_id UNIQUE (author_id),
CONSTRAINT UQ_ISBN_author_id UNIQUE(ISBN, author_id)
)
GO
ALTER TABLE [book_author]
 ADD CONSTRAINT CK_book_author_id CHECK (book_author_id >= 1)
GO
ALTER TABLE [book_author]
 ADD CONSTRAINT CK_book_author_idseq_no CHECK (seq_no >= 1)
GO

ALTER TABLE [book_author]
WITH CHECK ADD  CONSTRAINT FK_book_author_book FOREIGN KEY(ISBN)
REFERENCES [book] (ISBN)
ON UPDATE CASCADE
ON DELETE NO ACTION
GO
    
ALTER TABLE [book_author] 
WITH CHECK ADD  CONSTRAINT FK_book_author_author FOREIGN KEY(author_id)
REFERENCES [author] (author_id)
ON UPDATE NO ACTION
ON DELETE NO ACTION
 GO

     

CREATE TABLE [author_log] (
operation_id INT IDENTITY PRIMARY KEY,
author_id_new INT,
name_new NVARCHAR(50),
URL_new VARCHAR(50),
author_id_old INT,
name_old NVARCHAR(50),
URL_old VARCHAR(50),
operation_type CHAR(1) NOT NULL,
operation_datetime DATETIME NOT NULL DEFAULT GETDATE()
)
GO
