
USE [R_Kryvulia_Library]
GO
--DROP TRIGGER IF EXISTS [tr_authorUPD] 
--GO
DROP TRIGGER IF EXISTS [tr_publisherUPD] 
GO
DROP TRIGGER IF EXISTS [tr_bookUPD] 
GO
DROP TRIGGER IF EXISTS [tr_book_authorUPD] 
GO
DROP TRIGGER IF EXISTS [tr_authorIUD_ON_author_log]
GO

 --create trigger_update on table [publisher]
CREATE TRIGGER [tr_publisherUPD] ON [publisher]
AFTER UPDATE
AS
BEGIN
  UPDATE [publisher]
     SET updated = GETDATE(),
       updated_by  = system_user
     FROM [inserted]
     WHERE publisher.publisher_id = inserted.publisher_id
 END
 GO

 --create trigger_update on table [book]
CREATE TRIGGER [tr_bookUPD] ON [book]
AFTER UPDATE
AS
BEGIN
  UPDATE [book]
     SET updated = GETDATE(),
       updated_by  = system_user
     FROM [inserted]
     WHERE book.ISBN = inserted.ISBN
END
GO

--create trigger_update on table [book_author]
CREATE TRIGGER [tr_book_authorUPD] ON [book_author]
AFTER UPDATE
AS
BEGIN
  UPDATE [book_author]
     SET updated = GETDATE(),
       updated_by  = system_user
     FROM [inserted]
     WHERE book_author.book_author_id = inserted.book_author_id
END
GO

--create trigger_ins,del,upd on table [author]
create trigger [tr_authorIUD_ON_author_log] ON [author]
AFTER INSERT, UPDATE, DELETE
AS
BEGIN
--SET NOCOUNT ON;  set @@rowcount in 0
IF @@ROWCOUNT = 0 RETURN
DECLARE @operation char(1)
DECLARE @ins int = (select count(*) from INSERTED)
DECLARE @del int = (select count(*) from DELETED)
set @operation = 
case 
	when @ins > 0 and @del > 0 then 'U'  
	when @ins = 0 and @del > 0 then 'D'  
	when @ins > 0 and @del = 0 then 'I'  
end 
---- insert
if @operation = 'I'
begin

	insert into [author_log]
            (author_id_new, name_new, URL_new, author_id_old, name_old, URL_old, operation_type)
	Select 
		inserted.author_id, 
		inserted.name,
		inserted.URL,
		NULL,
		NULL,
		NULL,
		@operation 
		FROM [author]
	inner join inserted on [author].author_id = inserted.author_id
end

---- delete
if @operation = 'D'
begin
	insert into [author_log]
	(author_id_new, name_new, URL_new, author_id_old, name_old, URL_old, operation_type)
	Select 
		NULL,
		NULL,
		NULL,
		deleted.author_id, 
		deleted.name,
		deleted.URL,
		@operation 
	FROM deleted 
end
--- update (+update table [author])
if @operation = 'U'
begin
	 UPDATE [author]
     SET updated = GETDATE(),
       updated_by  = system_user
     FROM [inserted]
     WHERE author.author_id = inserted.author_id
 			insert into [author_log]
			(author_id_new, name_new, URL_new, author_id_old, name_old, URL_old, operation_type)
			select
		    inserted.author_id, 
		    inserted.name,
		    inserted.URL,
			deleted.author_id, 
		    deleted.name,
		    deleted.URL,
		    @operation
			FROM [author]
				 inner join  inserted on [author].author_id = inserted.author_id
				 inner join deleted on [author].author_id = deleted.author_id
				 			 
	end

END
GO

--sp_settriggerorder @triggername = '[tr_authorIUD_ON_author_log]', @order = 'first', @stmttype = 'UPDATE'
--sp_settriggerorder @triggername = '[tr_authorUPD]', @order = 'last', @stmttype = 'UPDATE'


