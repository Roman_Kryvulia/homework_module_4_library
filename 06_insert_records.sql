USE [R_Kryvulia_Library]
GO


INSERT INTO [author](author_id, name) VALUES 
(next value for gen_number, 'hasek'), (next value for gen_number, 'kostenko'), (next value for gen_number, 'rouling'), (next value for gen_number, 'sfift'),
(next value for gen_number, 'hohol'), (next value for gen_number, 'waild'), (next value for gen_number, 'shevchenko'), (next value for gen_number, 'sagaidak'),
(next value for gen_number, 'franko'), (next value for gen_number, 'twen'), (next value for gen_number, 'azimov'), (next value for gen_number, 'conan-doil'),
(next value for gen_number, 'bredberi'), (next value for gen_number, 'pokalchuk'), (next value for gen_number, 'lem'), (next value for gen_number, 'zhadan'),
(next value for gen_number, 'krajewski'), (next value for gen_number, 'bulhakov'), (next value for gen_number, 'lindgren'), (next value for gen_number, 'rozobudko')
GO

INSERT INTO [publisher](publisher_id, name) VALUES 
(next value for gen_number, 'caravela'), (next value for gen_number, 'new time'), (next value for gen_number, 'terra incognita'), (next value for gen_number, 'intelect'),
(next value for gen_number, 'a-ba-ba-ga-la-ma-ga'), (next value for gen_number, 'apostol'), (next value for gen_number, 'kalvaria'), (next value for gen_number, 'inter-book'),
(next value for gen_number, 'karpaty'), (next value for gen_number, 'knyga plus'), (next value for gen_number, 'stavropigion'), (next value for gen_number, 'lira-K'),
(next value for gen_number, 'klio'), (next value for gen_number, 'litera LTD'), (next value for gen_number, 'meta'), (next value for gen_number, 'molod'),
(next value for gen_number, 'nova knyga'), (next value for gen_number, 'nora-druk'), (next value for gen_number, 'makhaon'), (next value for gen_number, 'folio')
GO


INSERT INTO [book] (ISBN, publisher_id, URL, price)  VALUES 

('978-3-16-148410-1', '62', 'www.calvar.com.ua', 55), ('978-3-16-148410-4', '64', 'www.interbook.com.ua', 70),('978-3-16-145610-4', '64', 'www.interbooks.com.ua', 90),
('978-5-16-148327-2', '62', 'www.calvaria.com.ua', 98), ('978-3-16-156410-7', '58', 'www.baba.org', 170),('978-4-16-122410-4', '74', 'www.interbookk.com.ua', 120),
('978-5-16-148100-3', '60', 'www.apostol.com.ua', 132), ('978-3-16-150010-4', '60', 'www.apoba.org', 130),('978-4-16-122140-9', '66', 'www.inbook.com.ua', 220),
('978-6-16-148100-3', '74', 'www.nora.com.ua', 132), ('978-6-16-150010-4', '76', 'www.druk.org', 130),('978-6-16-122140-9', '78', 'www.sbook.com.ua', 120),
('978-4-16-148100-5', '70', 'www.stolka.com.ua', 110), ('978-4-16-150340-6', '62', 'www.obar.org', 270),('978-5-26-129840-7', '84', 'www.insbook.com.ua', 150),
('978-9-16-148500-5', '66', 'www.stol.org.ua', 45), ('978-8-16-126010-6', '68', 'www.aba.org', 350)
GO
INSERT INTO [book] (ISBN, publisher_id, URL, price)  VALUES 
('978-3-16-140010-3', '60', 'www.calqvar.com.ua', 250), ('978-3-16-140810-3', '72', 'www.intbook.com.ua', 370),('978-3-16-140410-3', '66', 'www.inbooks.com.ua', 70)
GO
INSERT INTO [book_author] (book_author_id, ISBN, author_id, seq_no)  VALUES
(1, '978-3-16-148410-1', 10, 5),(2, '978-5-16-148327-2', 12, 7), (3, '978-5-16-148100-3', 14, 8), (4, '978-6-16-148100-3', 16, 9),
(5, '978-4-16-148100-5', 18, 55), (6, '978-9-16-148500-5', 20, 45), (7, '978-3-16-148410-4', 22, 80), (8, '978-3-16-156410-7', 24, 19),
(9, '978-3-16-150010-4', 26, 155), (10, '978-6-16-150010-4', 28, 145), (11, '978-4-16-150340-6', 30, 26), (12, '978-8-16-126010-6', 32, 21),
(13, '978-3-16-145610-4', 34, 105), (14, '978-4-16-122410-4', 36, 15), (15, '978-4-16-122140-9', 38, 3), (16, '978-6-16-122140-9', 40, 24),
(17, '978-5-26-129840-7', 42, 5), (18, '978-3-16-140010-3', 44, 19), (42, '978-3-16-140810-3', 46, 30), (20, '978-3-16-140410-3', 48, 54)
GO

SELECT * FROM [author]
SELECT * FROM [publisher]
SELECT * FROM [book]
SELECT * FROM [book_author]
GO
