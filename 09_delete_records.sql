USE [R_Kryvulia_Library]
GO


DELETE [book_author] 
WHERE author_id >= 40
GO

DELETE [author] 
WHERE author_id >= 40
GO

DELETE [book] 
WHERE ISBN IN ('978-3-16-140410-3', '978-3-16-140810-3', '978-3-16-140010-3', '978-5-26-129840-7', '978-6-16-122140-9') 
GO

DELETE [publisher] 
WHERE publisher_id >= 80
GO




SELECT * FROM [book_author]
SELECT * FROM [author]
SELECT * FROM [book]
SELECT * FROM [publisher]
SELECT * FROM [author_log]